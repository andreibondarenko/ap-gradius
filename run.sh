#!/bin/bash

if [ ! -f Gradius.out ]; then
	cmake .
	make
	rm -f cmake_install.cmake CMakeCache.txt Makefile
	rm -rf CMakeFiles
	clear
fi

echo -e "Available levels:"
ls res/levels
echo -e "Please select one of the levels:"
read filename
LEVEL="res/levels/$filename"
echo -e $LEVEL
if [ ! -f $LEVEL ]; then
	echo -e "The file you selected does not exist!"
else
	eval ./Gradius.out res/settings.json $LEVEL
fi