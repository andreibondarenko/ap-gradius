#ifndef GRADIUS_SETTINGS_HPP
#define GRADIUS_SETTINGS_HPP

#include <array>
#include "Template/Singleton.hpp"

struct Settings : public Singleton<Settings>
{
public:

    /**
     * @brief Loads settings from file
     * @param filename Path to the settings file
     */
    bool load_from_file(std::string const &filename);

    // Window settings
    unsigned int resolution_width = 800;
    unsigned int resolution_height = 600;
    bool full_screen_enabled = false;

    // Model settings
    const float MODEL_WIDTH = 8.0f;
    const float MODEL_HEIGHT = 6.0f;
    const float SCROLL_SPEED = 0.03f;

    // Entity settings
    const float MOVEMENT_SPEED_SHIPS = 0.05f;
    const float MOVEMENT_SPEED_BULLETS = 0.125f;
    const float SIZE_SHIPS = 0.25f;
    const float SIZE_BULLETS = 0.05f;
    const float SIZE_HP = 0.2f;
    const float SIZE_BORDER = 1;
    const int ENEMY_SHOT_INTERVAL = 50;
    const int BACKGROUND_Z= -1;
    const int GAME_Z= 0;
    const int UI_Z= 1;
    const int MAX_HP = 5;

    // Init positions
    const float HP_Y_POS = -2.8f;
    const std::array<float, 5> HP_X_POS = {-3.8f, -3.5f, -3.2f, -2.9f, -2.6f};
    const float SHIP_INIT_X = -2.5f;
    const std::array<float, 3> BG_X_POS = {-1, 5, 11};
    const std::array<float, 9> BORDER_X_POS = {-3.5f, -2.5f, -1.5f, -0.5f, 0.5f, 1.5f, 2.5f, 3.5f, 4.5f};
    const float TOP_BORDER_Y_POS = 3.125f;
    const float BOTTOM_BORDER_Y_POS = -3.125f;


private:
    /** Gives Singleton template access to private default constructor */
    friend class Singleton<Settings>;
    /**
     * @brief Default constructor
     * @details Default constructor, private to satisfy singleton pattern
     */
    Settings() = default;
    /**
     * @brief Copy constructor
     * @details Copy constructor, deleted and private to satisfy singleton pattern
     */
    Settings(Settings const& s) = delete;
    /**
     * @brief Assignment operator
     * @details Assignment operator, deleted and private to satisfy singleton pattern
     */
    Settings& operator=(Settings const& s) = delete;
};


#endif //GRADIUS_SETTINGS_HPP
