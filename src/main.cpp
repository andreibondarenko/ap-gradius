#include "View/Window.hpp"
#include "Controller.hpp"
#include "Model/Game.hpp"
#include "Settings.hpp"
#include <thread>
#include <future>
#include <iostream>

int main(int argc, char const *argv[])
{
    Model::Game model;
    Controller control;
    View::Window view;

    if (!Settings::get_instance().load_from_file(argv[1]))
        return -1;
    if (!model.load_from_file(argv[2]))
        return -1;

    control.link_model(model);
    model.link_controller(control);
    model.link_view(view);
    view.link_controller(control);

    std::thread model_thread(&Model::Game::run, &model);
    view.run();
    model_thread.join();

    return 0;
}