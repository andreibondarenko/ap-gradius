#ifndef GRADIUS_CONTROLLER_HPP
#define GRADIUS_CONTROLLER_HPP

#include <memory>
#include <queue>
#include <SFML/Graphics.hpp>

namespace Model
{
    class Game;
}

class Controller
{
public:
    /**
     * @brief Default constructor
     */
    Controller() = default;
    /**
     * @brief Copy constructor
     * @details Copy constructor, deleted to prevent copies
     */
    Controller(Controller const& view_controller) = delete;
    /**
     * @brief Assignment operator
     * @details Assignment operator, deleted to prevent assignment
     */
    Controller& operator=(Controller const& view_controller) = delete;
    /**
     * @brief Links game model to controller
     * @param new_model model to link the controller to
     */
    void link_model(Model::Game& new_model);
    /**
     * @brief Add input event to event queue
     * @param new_event input event
     */
    void register_event(sf::Event new_event);
    /**
     * @brief Sends the appropriate signal to model based on the event at front of queue
     */
    void process_next_event();
    /**
     * @brief Stops the model from running
     */
    void terminate_model();

private:

    std::queue<sf::Event> event_queue;
    /**
     * Reason for using raw pointer over shared pointer:
     * Since model is instantiated separately, destruction will be taken care of
     * by the model itself when exiting main.
     * If we create a shared pointer to an already instantiated object, we will
     * get Malloc errors when the destructor of the shared pointer is invoked
     */
    Model::Game* model = nullptr;


};

#endif //GRADIUS_CONTROLLER_HPP
