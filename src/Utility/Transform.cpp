#include "Transform.hpp"
#include "../Settings.hpp"

float Utility::Transform::x_model_to_view(float x_model)
{
    return (Settings::get_instance().resolution_width/Settings::get_instance().MODEL_WIDTH)
           *
           (x_model+(Settings::get_instance().MODEL_WIDTH/2.0f));
}

float Utility::Transform::y_model_to_view(float y_model)
{
    return Settings::get_instance().resolution_height
           -
           (
                   (Settings::get_instance().resolution_height/Settings::get_instance().MODEL_HEIGHT)
                   *
                   (y_model+(Settings::get_instance().MODEL_HEIGHT/2.0f))
           );
}

float Utility::Transform::x_view_to_model(float x_view)
{
    return ((Settings::get_instance().MODEL_WIDTH/Settings::get_instance().resolution_width)*x_view)
           -
           (Settings::get_instance().MODEL_WIDTH/2.0f);
}

float Utility::Transform::y_view_to_model(float y_view)
{
    return (
                   (Settings::get_instance().resolution_height-y_view)
                   *
                   (Settings::get_instance().MODEL_HEIGHT/Settings::get_instance().resolution_height)
           )
           -
           (Settings::get_instance().MODEL_HEIGHT/2.0f);
}

float Utility::Transform::get_x_scale_factor(float model_size_x, int texture_size)
{
    float view_size = (model_size_x*Settings::get_instance().resolution_width)/8.0f;
    float scale_factor = view_size/(float)texture_size;
    return scale_factor;
}

float Utility::Transform::get_y_scale_factor(float model_size_y, int texture_size)
{
    float view_size = (model_size_y*Settings::get_instance().resolution_height)/6.0f;
    float scale_factor = view_size/(float)texture_size;
    return scale_factor;
}