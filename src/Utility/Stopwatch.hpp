#ifndef GRADIUS_STOPWATCH_HPP
#define GRADIUS_STOPWATCH_HPP

#include <chrono>
#include "../Template/Singleton.hpp"

namespace Utility
{
    class Stopwatch : public Singleton<Stopwatch>
    {
    public:
        /**
         * @brief Checks whether enough time has elapsed for the next tick
         * @return bool indicating whether or not enough time elpased
         */
        bool is_next_tick();

    private:
        /** Gives Singleton template access to private default constructor */
        friend class Singleton<Stopwatch>;
        /**
         * @brief Default constructor
         * @details Default constructor, private to satisfy singleton pattern
         */
        Stopwatch();
        /**
         * @brief Copy constructor
         * @details Copy constructor, deleted and private to satisfy singleton pattern
         */
        Stopwatch(Stopwatch const& s) = delete;
        /**
         * @brief Assignment operator
         * @details Assignment operator, deleted and private to satisfy singleton pattern
         */
        Stopwatch& operator=(Stopwatch const& s) = delete;

        std::chrono::steady_clock::time_point last_timestamp;

    };
}

#endif //GRADIUS_STOPWATCH_HPP
