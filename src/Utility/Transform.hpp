#ifndef GRADIUS_TRANSFORM_HPP
#define GRADIUS_TRANSFORM_HPP


#include <SFML/Graphics.hpp>
#include "../Template/Singleton.hpp"

namespace Utility
{
    class Transform : public Singleton<Transform>
    {
    public:
        /**
         * @brief Convert x-coordinate from model to view
         *
         * @param x_model original x-coordinate (model real)
         * @return result x-coordinate (view pixel)
         */
        float x_model_to_view(float x_model);
        /**
         * @brief Convert y-coordinate from model to view
         *
         * @param y_model original y-coordinate (model real)
         * @return result y-coordinate (view pixel)
         */
        float y_model_to_view(float y_model);
        /**
         * @brief Convert x-coordinate from view to model
         *
         * @param x_view original x-coordinate (view pixel)
         * @return result x-coordinate (model real)
         */
        float x_view_to_model(float x_view);
        /**
         * @brief Convert y-coordinate from view to model
         *
         * @param x_view original y-coordinate (view pixel)
         * @return result y-coordinate (model real)
         */
        float y_view_to_model(float y_view);
        /**
         * @brief Determines horizontal scale factor
         * @param model_size_x width of the entity in the model
         * @param texture_size size of the texture
         */
        float get_x_scale_factor(float model_size_x, int texture_size);
        /**
         * @brief Determines vertical scale factor
         * @param model_size_y height of the entity in the model
         * @param texture_size size of the texture
         */
        float get_y_scale_factor(float model_size_y, int texture_size);

    private:
        /** Gives Singleton template access to private default constructor */
        friend class Singleton<Transform>;
        /**
         * @brief Default constructor
         * @details Default constructor, private to satisfy singleton pattern
         */
        Transform() = default;
        /**
         * @brief Copy constructor
         * @details Copy constructor, deleted and private to satisfy singleton pattern
         */
        Transform(Transform const& t) = delete;
        /**
         * @brief Assignment operator
         * @details Assignment operator, deleted and private to satisfy singleton pattern
         */
        Transform& operator=(Transform const& t) = delete;

    };
}



#endif //GRADIUS_TRANSFORM_HPP
