#include "Stopwatch.hpp"

bool Utility::Stopwatch::is_next_tick()
{
    auto current_time = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed_seconds = current_time-last_timestamp;
    if (elapsed_seconds.count() >= (1.0/60.0))
    {
        last_timestamp = current_time;
        return true;
    }
    return false;
}

Utility::Stopwatch::Stopwatch()
{
    last_timestamp = std::chrono::steady_clock::now();
}
