#ifndef GRADIUS_PLAYER_HPP
#define GRADIUS_PLAYER_HPP

#include <memory>
#include <array>
#include <stack>
#include "Entity.hpp"
#include "HealthPoint.hpp"

namespace Model
{
    class Player : public Entity
    {
    public:
        /**
         * @brief Default constructor
         */
        Player();
        /**
         * @brief Construct the player at given location, with given contact radius
         * @param init_x initial position on x-axis
         * @param init_y initial position on y-axis
         */
        Player(float init_x, float init_y);
        /**
         * @brief Destructor
         */
        ~Player() override = default;
        /**
         * @brief Copy constructor
         */
        Player(Player const& player) = default;
        /**
         * @brief Assignment operator
         */
        Player& operator=(Player const& player) = default;
        /**
         * @brief Creates and adds a health point to the player
         * @return A pointer to health point entity
         */
        std::shared_ptr<Model::Entity> add_health_point();
        /**
         * @brief Removes a number of health points from the player
         * @param points number of points to remove
         */
        void remove_health_point(int points);
        /**
         * @brief Creates and shoots a bullet
         * @return A pointer to the bullet entity
         */
        std::shared_ptr<Model::Entity> shoot() const;
        /**
         * @brief Indicates the type of the entity
         * @return EntityType enum value indicating instance is a player entity
         */
        EntityType get_type() const override;
        /**
         * @brief Progresses entity one tick further
         * @param entity_list List of all entities in the model, used when entities create other entities e.g. bullets
         */
        void tick(std::list<std::shared_ptr<Model::Entity>> &entity_list) override;
        /**
         * @brief Resets the player to the starting position
         */
        void rebound(Direction rebound_direction);
        /**
         * @brief Indicates if player is still alive
         * @return true if player still has health points, false otherwise
         */
        bool is_alive();

    private:
        std::stack<std::shared_ptr<Model::HealthPoint>> health_points;
    };
}

#endif //GRADIUS_PLAYER_HPP
