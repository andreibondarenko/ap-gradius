#ifndef GRADIUS_SUBJECT_HPP
#define GRADIUS_SUBJECT_HPP

#include <memory>
#include <vector>
#include "../View/Observer.hpp"

namespace Abstract
{
    class Subject
    {
    public:
        /**
         * @brief Notifies observers of updates
         */
        virtual void notify_observers() = 0;
        /**
         * @brief Attaches an observer to the subject
         *
         * @param  Pointer to observer
         */
        void attach_observer(std::shared_ptr<Observer> new_observer);
        /**
         * @brief Detaches any observers the subject has
         */
        void clear_observers();
        /**
         * @brief Indicates whether or not subject has any observers
         * @return True if subject has observers, false otherwise
         */
        bool has_observers() const;

    protected:
        /**
         * @brief Default constructor
         */
        Subject() = default;
        /**
         * @brief Pure virtual destructor, prevents direct instantiation of Subject
         */
        virtual ~Subject() = 0;
        /**
         * @brief Copy constructor
         */
        Subject(Subject const& s) = default;
        /**
         * @brief Assignment operator
         */
        Subject& operator=(Subject const& s) = default;

        std::vector<std::shared_ptr<Observer>> observer_collection;

    };
}

#endif //GRADIUS_SUBJECT_HPP
