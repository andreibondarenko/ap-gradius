#ifndef GRADIUS_ENEMYUFO_HPP
#define GRADIUS_ENEMYUFO_HPP

#include "Entity.hpp"

namespace Model
{
    class EnemyUFO : public Entity
    {
    public:
        /**
         * @brief Default constructor
         */
        EnemyUFO();
        /**
         * @brief Construct the enemy at given location
         * @param init_x initial position on x-axis
         * @param init_y initial position on y-axis
         */
        EnemyUFO(float init_x, float init_y);
        /**
         * @brief Destructor
         */
        ~EnemyUFO() override = default;
        /**
         * @brief Copy constructor
         */
        EnemyUFO(EnemyUFO const& player) = default;
        /**
         * @brief Assignment operator
         */
        EnemyUFO& operator=(EnemyUFO const& player) = default;
        /**
         * @brief Progresses entity one tick further
         * @param entity_list List of all entities in the model, used when entities create other entities e.g. bullets
         */
        void tick(std::list<std::shared_ptr<Entity>> &entity_list) override;
        /**
         * @brief Indicates the type of the entity
         * @return EntityType enum value indicating instance is a bullet entity
         */
        EntityType get_type() const override;
        /**
         * @brief Lowers the enemies health by given number of points
         * @param points Numbers of health points to subtract
         */
        void remove_health_point(int points);

    private:
        int health_points = 2;

    };
}

#endif //GRADIUS_ENEMYUFO_HPP
