#ifndef GRADIUS_ENEMYFIGHTER_HPP
#define GRADIUS_ENEMYFIGHTER_HPP

#include "Entity.hpp"

namespace Model
{
    class EnemyFighter : public Entity
    {
    public:
        /**
         * @brief Default constructor
         */
        EnemyFighter();
        /**
         * @brief Construct the player at given location, with given contact radius
         * @param init_x initial position on x-axis
         * @param init_y initial position on y-axis
         * @param cr contact radius/hitbox-size for player
         */
        EnemyFighter(float init_x, float init_y);
        /**
         * @brief Destructor
         */
        ~EnemyFighter() override = default;
        /**
         * @brief Copy constructor
         */
        EnemyFighter(EnemyFighter const& player) = default;
        /**
         * @brief Assignment operator
         */
        EnemyFighter& operator=(EnemyFighter const& player) = default;
        /**
         * @brief Progresses entity one tick further
         * @param entity_list List of all entities in the model, used when entities create other entities e.g. bullets
         */
        void tick(std::list<std::shared_ptr<Entity>> &entity_list) override;
        /**
         * @brief Indicates the type of the entity
         * @return EntityType enum value indicating instance is a bullet entity
         */
        EntityType get_type() const override;
        /**
         * @brief Lowers the enemies health by given number of points
         * @param points Numbers of health points to subtract
         */
        void remove_health_point(int points);

    private:
        /**
         * @brief Make enemy shoot a bullet
         * @return Pointer to created bullet entity
         */
        std::shared_ptr<Model::Entity> shoot() const;

        int health_points = 1;
        int shot_timer = 0;
    };
}

#endif //GRADIUS_ENEMYFIGHTER_HPP
