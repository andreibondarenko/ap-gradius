#ifndef GRADIUS_OBSTACLE_HPP
#define GRADIUS_OBSTACLE_HPP


#include "Entity.hpp"

namespace Model
{
    class Obstacle : public Entity
    {
    public:
        /**
         * @brief Default constructor
         */
        Obstacle();
        /**
         * @brief Construct background at given location
         * @param init_x initial position on x-axis
         * @param init_y initial position on y-axis
         */
        Obstacle(float init_x, float init_y, float size);
        /**
         * @brief Destructor
         */
        ~Obstacle() override = default;
        /**
         * @brief Copy constructor
         */
        Obstacle(Obstacle const& b) = default;
        /**
         * @brief Assignment operator
         */
        Obstacle& operator=(Obstacle const& b) = default;
        /**
         * @brief Indicates the type of the entity
         * @return EntityType enum value indicating instance is a obstacle entity
         */
        EntityType get_type() const override;
    };
}


#endif //GRADIUS_OBSTACLE_HPP
