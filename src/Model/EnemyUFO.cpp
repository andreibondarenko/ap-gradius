#include "EnemyUFO.hpp"
#include "../Settings.hpp"

Model::EnemyUFO::EnemyUFO() : Entity(0,
                                     0,
                                     Settings::get_instance().GAME_Z,
                                     Settings::get_instance().SIZE_SHIPS)
{
    friendly = false;
    x_speed = -Settings::get_instance().SCROLL_SPEED;
}

Model::EnemyUFO::EnemyUFO(float init_x, float init_y) : Entity(init_x,
                                                               init_y,
                                                               Settings::get_instance().GAME_Z,
                                                               Settings::get_instance().SIZE_SHIPS)
{
    friendly = false;
    x_speed = -Settings::get_instance().SCROLL_SPEED;
}

void Model::EnemyUFO::tick(std::list<std::shared_ptr<Model::Entity>> &entity_list)
{
    Entity::tick(entity_list);
    if (in_fov() && x_speed == -Settings::get_instance().SCROLL_SPEED)
    {
        x_speed = 0;
        speed_up(Direction::left);
    }
    if (x + Settings::get_instance().SIZE_SHIPS < -Settings::get_instance().MODEL_WIDTH/2.0f)
        deleted = true;
}

Model::EntityType Model::EnemyUFO::get_type() const
{
    return EntityType::enemy_ufo_entity;
}

void Model::EnemyUFO::remove_health_point(int points)
{
    health_points -= points;
    if (health_points <= 0)
        deleted = true;
}
