#include "Game.hpp"
#include "../Lib/json.hpp"
#include "../Settings.hpp"
#include "../Utility/Stopwatch.hpp"
#include "../Controller.hpp"
#include "../View/Window.hpp"
#include "Background.hpp"
#include "Border.hpp"
#include "EnemyFighter.hpp"
#include "EnemyUFO.hpp"
#include "Obstacle.hpp"
#include <fstream>

using json = nlohmann::json;

Model::Game::Game()
{
    /** Elementary level setup */
    /** Player init */
    player = std::shared_ptr<Player>(new Player(Settings::get_instance().SHIP_INIT_X, 0));
    entities.push_back(player);
    for (int i = 0; i < Settings::get_instance().MAX_HP; ++i)
    {
        entities.push_back(player->add_health_point());
    }
    /** Background init */
    for (auto const& x_pos: Settings::get_instance().BG_X_POS)
    {
        entities.push_back(std::shared_ptr<Background>(new Background(x_pos, 0)));
    }
    /** Borders init */
    for (auto const& x_pos: Settings::get_instance().BORDER_X_POS)
    {
        entities.push_back(std::shared_ptr<Border>(new Border(x_pos, Settings::get_instance().TOP_BORDER_Y_POS)));
        entities.push_back(std::shared_ptr<Border>(new Border(x_pos, Settings::get_instance().BOTTOM_BORDER_Y_POS)));
    }
}

bool Model::Game::load_from_file(std::string const &lvl_file)
{
    // START Parse .json input file
    json data;
    std::ifstream JSON(lvl_file);
    if (JSON.is_open())
    {
        JSON >> data;
        JSON.close();
    }
    else
    {
        std::cout << "Unable to open file: "<< lvl_file << std::endl;
        return false;
    }
    // END Parse .json input file

    int invalid_obstacles = 0;
    int invalid_fighters = 0;
    int invalid_ufos = 0;

    try
    {
        for (auto const& obstacle: data.at("obstacles"))
        {
            try
            {
                entities.push_back(std::shared_ptr<Obstacle>(new Obstacle(obstacle.at("X"),
                                                                          obstacle.at("Y"),
                                                                          obstacle.at("SIZE"))));
            }
            catch (std::out_of_range const& oor)
            {
                invalid_obstacles++;
                std::cout << obstacle << " is not a valid obstacle!" << std::endl;
            }
        }
        for (auto const& fighter: data.at("fighters"))
        {
            try
            {
                entities.push_back(std::shared_ptr<EnemyFighter>(new EnemyFighter(fighter.at("X"),
                                                                                  fighter.at("Y"))));
            }
            catch (std::out_of_range const& oor)
            {
                invalid_fighters++;
                std::cout << fighter << " is not a valid fighter!" << std::endl;
            }
        }
        for (auto const& ufo: data.at("ufos"))
        {
            try
            {
                entities.push_back(std::shared_ptr<EnemyUFO>(new EnemyUFO(ufo.at("X"),
                                                                          ufo.at("Y"))));
            }
            catch (std::out_of_range const& oor)
            {
                invalid_ufos++;
                std::cout << ufo << " is not a valid ufo!" << std::endl;
            }
        }
        end_marker = data.at("end");
    }
    catch (std::out_of_range const& oor)
    {
        std::cout << "FATAL ERROR: Level file invalid" << std::endl;
        return false;
    }

    std::cout << "Level loaded..." << std::endl;
    if (invalid_fighters && invalid_obstacles && invalid_ufos)
    {
        std::cout << "Level may be incomplete because invalid objects were detected:"
                  << std::endl
                  << "-Invalid obstacles: "
                  << invalid_obstacles
                  << std::endl
                  << "-Invalid fighters: "
                  << invalid_fighters
                  << std::endl
                  << "-Invalid ufos: "
                  << invalid_ufos
                  << std::endl;
    }
    return true;
}

void Model::Game::execute_command(Model::Command command)
{
    switch (command)
    {
        case Command::start_moving_up:
            player->speed_up(Direction::up);
            break;
        case Command::start_moving_down:
            player->speed_up(Direction::down);
            break;
        case Command::start_moving_left:
            player->speed_up(Direction::left);
            break;
        case Command::start_moving_right:
            player->speed_up(Direction::right);
            break;
        case Command::stop_moving_up:
            player->slow_down(Direction::up);
            break;
        case Command::stop_moving_down:
            player->slow_down(Direction::down);
            break;
        case Command::stop_moving_left:
            player->slow_down(Direction::left);
            break;
        case Command::stop_moving_right:
            player->slow_down(Direction::right);
            break;
        case Command::player_shoot:
            entities.push_back(player->shoot());
            break;
    }
}

bool Model::Game::run()
{
    running = true;
    while (running)
    {
        if (Utility::Stopwatch::get_instance().is_next_tick())
        {
            end_marker -= Settings::get_instance().SCROLL_SPEED;
            control->process_next_event();
            for (auto ptr_it = entities.begin(); ptr_it != entities.end();)
            {
                (*ptr_it)->tick(entities);
                (*ptr_it)->notify_observers();
                if ((*ptr_it)->is_destroyed())
                {
                    ptr_it = entities.erase(ptr_it);
                    continue;
                }
                else if ((*ptr_it)->in_fov() && !(*ptr_it)->has_observers())
                {
                    window->create_representation((*ptr_it));
                }
                else if (!(*ptr_it)->in_fov() && (*ptr_it)->has_observers())
                {
                    (*ptr_it)->clear_observers();
                }
                ++ptr_it;
            }
            detect_collisions();
        }
        if (end_marker < -Settings::get_instance().MODEL_WIDTH/2.0f)
        {
            this->stop();
            window->terminate();
            std::cout << "Congratulations! You beat the level!" << std::endl;
            return true;
        }
        if (!player->is_alive())
        {
            this->stop();
            window->terminate();
            std::cout << "Game Over! You didn't beat the level!" << std::endl;
            return false;
        }
    }
    return false;
}

void Model::Game::stop()
{
    running = false;
}

void Model::Game::link_controller(Controller& c)
{
    control = &c;
}

void Model::Game::link_view(View::Window &w)
{
    window = &w;
}

void Model::Game::detect_collisions()
{
    for (auto entIt1 = entities.begin(); entIt1 != entities.end(); ++entIt1)
    {
        if ((*entIt1)->in_fov() && (*entIt1)->is_collidable())
        {
            for (auto entIt2 = std::next(entIt1); entIt2 != entities.end(); ++entIt2)
            {
                if ((*entIt2)->in_fov() && (*entIt2)->is_collidable())
                {
                    /** We use squared values to circumvent slower square root calculation */
                    float d_squared = pow((*entIt2)->get_x() - (*entIt1)->get_x(), 2.0f)
                                      + pow((*entIt2)->get_y() - (*entIt1)->get_y(), 2.0f);
                    float size_sum_squared = pow((*entIt1)->get_size() + (*entIt2)->get_size(), 2.0f);
                    if (size_sum_squared >= d_squared)
                    {
                        handle_collision((*entIt1), (*entIt2));
                    }
                }
            }
        }
    }
}

void Model::Game::handle_collision(std::shared_ptr<Model::Entity> ent1, std::shared_ptr<Model::Entity> ent2)
{
    EntityType type1 = ent1->get_type();
    EntityType type2 = ent2->get_type();

    if (ent1->is_friendly() && ent2->is_friendly())
        return;
//     Make sure ent1 is always friendly,
//     or if both entities are non-friendly that following order for
//     ent1 < ent2 is always followed: ship < bullet < environment
    if (!ent1->is_friendly())
    {
        std::swap(ent1, ent2);
        std::swap(type1, type2);
    }
    if (!ent1->is_friendly() && !ent2->is_friendly() && type1 > type2)
    {
        std::swap(ent1, ent2);
        std::swap(type1, type2);
    }

    switch (type1)
    {
        case EntityType::player_entity:
            switch (type2)
            {
                case EntityType::top_border_entity:
                    std::static_pointer_cast<Model::Player>(ent1)->remove_health_point(2);
                    std::static_pointer_cast<Model::Player>(ent1)->rebound(Direction::down);
                    break;
                case EntityType::bottom_border_entity:
                    std::static_pointer_cast<Model::Player>(ent1)->remove_health_point(2);
                    std::static_pointer_cast<Model::Player>(ent1)->rebound(Direction::up);
                    break;
                case EntityType::obstacle_entity:
                    std::static_pointer_cast<Model::Player>(ent1)->remove_health_point(1);
                    if (ent1->get_x() < ent2->get_x())
                        std::static_pointer_cast<Model::Player>(ent1)->rebound(Direction::left);
                    else
                        std::static_pointer_cast<Model::Player>(ent1)->rebound(Direction::right);
                    break;
                case EntityType::enemy_fighter_entity:
                    std::static_pointer_cast<Model::Player>(ent1)->remove_health_point(1);
                    ent2->destroy();
                    break;
                case EntityType::enemy_ufo_entity:
                    std::static_pointer_cast<Model::Player>(ent1)->remove_health_point(1);
                    ent2->destroy();
                    break;
                case EntityType::enemy_bullet_entity:
                    std::static_pointer_cast<Model::Player>(ent1)->remove_health_point(1);
                    ent2->destroy();
                    break;
                default:
                    break;
            }
            break;
        case EntityType::player_bullet_entity:
            switch (type2)
            {
                case EntityType::obstacle_entity:
                    ent1->destroy();
                    break;
                case EntityType::enemy_fighter_entity:
                    ent1->destroy();
                    std::static_pointer_cast<Model::EnemyFighter>(ent2)->remove_health_point(1);
                    break;
                case EntityType::enemy_ufo_entity:
                    ent1->destroy();
                    std::static_pointer_cast<Model::EnemyUFO>(ent2)->remove_health_point(1);
                    break;
                default:
                    break;
            }
            break;
        case EntityType::enemy_fighter_entity:
            switch (type2)
            {
                case EntityType::top_border_entity:
                    if (ent1->get_y_speed() > 0)
                    {
                        ent1->slow_down(Direction::up);
                        ent1->speed_up(Direction::down);
                    }
                    break;
                case EntityType::bottom_border_entity:
                    if (ent1->get_y_speed() < 0)
                    {
                        ent1->slow_down(Direction::down);
                        ent1->speed_up(Direction::up);
                    }
                    break;
                case EntityType::obstacle_entity:
                    if (ent1->get_y() > ent2->get_y())
                    {
                        if (ent1->get_y_speed() < 0)
                        {
                            ent1->slow_down(Direction::down);
                            ent1->speed_up(Direction::up);
                        }
                    }
                    else
                    {
                        if (ent1->get_y_speed() > 0)
                        {
                            ent1->slow_down(Direction::up);
                            ent1->speed_up(Direction::down);
                        }
                    }
                    break;
                default:
                    break;
            }
            break;
        case EntityType::enemy_ufo_entity:
            switch (type2)
            {
                case EntityType::obstacle_entity:
                    ent1->destroy();
                    break;
                default:
                    break;
            }
        case EntityType::enemy_bullet_entity:
            switch (type2)
            {
                case EntityType::obstacle_entity:
                    ent1->destroy();
                    break;
                default:
                    break;
            }
        default:
            break;
    }
}
