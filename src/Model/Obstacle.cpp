#include "Obstacle.hpp"
#include "../Settings.hpp"

Model::Obstacle::Obstacle() : Entity(0,
                                     0,
                                     Settings::get_instance().GAME_Z,
                                     0)
{
    friendly = false;
    x_speed = -Settings::get_instance().SCROLL_SPEED;
}

Model::Obstacle::Obstacle(float init_x, float init_y, float size) : Entity(init_x,
                                                                           init_y,
                                                                           Settings::get_instance().GAME_Z,
                                                                           size)
{
    friendly = false;
    x_speed = -Settings::get_instance().SCROLL_SPEED;
}

Model::EntityType Model::Obstacle::get_type() const
{
    return EntityType::obstacle_entity;
}
