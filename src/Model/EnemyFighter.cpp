#include "EnemyFighter.hpp"
#include "Bullet.hpp"
#include "../Settings.hpp"

Model::EnemyFighter::EnemyFighter() : Entity(0,
                                             0,
                                             Settings::get_instance().GAME_Z,
                                             Settings::get_instance().SIZE_SHIPS)
{
    friendly = false;
    x_speed = -Settings::get_instance().SCROLL_SPEED;
}

Model::EnemyFighter::EnemyFighter(float init_x, float init_y) : Entity(init_x,
                                                                       init_y,
                                                                       Settings::get_instance().GAME_Z,
                                                                       Settings::get_instance().SIZE_SHIPS)
{
    friendly = false;
    x_speed = -Settings::get_instance().SCROLL_SPEED;
}

Model::EntityType Model::EnemyFighter::get_type() const
{
    return EntityType::enemy_fighter_entity;
}

void Model::EnemyFighter::tick(std::list<std::shared_ptr<Model::Entity>> &entity_list)
{
    Entity::tick(entity_list);
    if (in_fov() && y_speed == 0)
    {
        if (y > 0)
            speed_up(Direction::up);
        else
            speed_up(Direction::down);
    }
    if (in_fov() && shot_timer++ == Settings::get_instance().ENEMY_SHOT_INTERVAL)
    {
        shot_timer = 0;
        entity_list.push_back(this->shoot());
    }
    if (x + Settings::get_instance().SIZE_SHIPS < -Settings::get_instance().MODEL_WIDTH/2.0f)
        deleted = true;
}

void Model::EnemyFighter::remove_health_point(int points)
{
    health_points -= points;
    if (health_points <= 0)
        deleted = true;
}

std::shared_ptr<Model::Entity> Model::EnemyFighter::shoot() const
{
    return std::shared_ptr<Model::Bullet>(new Bullet(this->x - this->size, this->y, true));
}
