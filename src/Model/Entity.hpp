#ifndef GRADIUS_ENTITY_HPP
#define GRADIUS_ENTITY_HPP

#include <list>
#include "Subject.hpp"

namespace Model
{
    /// Enum listing all valid types of entities for Model
    enum class EntityType
    {
        player_entity,          /// Player ship
        enemy_fighter_entity,   /// Enemy up-and-down, shooting moving fighter ship
        enemy_ufo_entity,       /// Enemy UFO ship
        player_bullet_entity,   /// Bullet shot by player
        enemy_bullet_entity,    /// Bullet shot by enemies
        top_border_entity,      /// Top border block
        bottom_border_entity,   /// Bottom border block
        obstacle_entity,        /// Obstacle block
        background_entity,      /// Background "block"
        hp_entity               /// HP indicator
    };

    /// Enum listing all valid directions an Entity can move in
    enum class Direction
    {
        up,     /// Up
        down,   /// Down
        left,   /// Left
        right   /// Right
    };

    class Entity : public Abstract::Subject
    {
    public:
        /**
         * @brief Getter for x-value private data member
         * @return the entities position on the x-axis
         */
        float get_x() const;
        /**
         * @brief Getter for y-value private data member
         * @return the entities position on the y-axis
         */
        float get_y() const;
        /**
         * @brief Getter for z-value private data member
         * @return order in which entity should be drawn
         */
        int get_z() const;
        /**
         * @brief Getter for size private data member
         * @return the entities radius
         */
        float get_size() const;
        /**
         * @brief Getter for x_speed private data member
         * @return the entities horizontal speed
         */
        float get_x_speed() const;
        /**
         * @brief Getter for y_speed private data member
         * @return the entities vertical speed
         */
        float get_y_speed() const;
        /**
         * @brief Speed up the entity in given direction
         * @param direction Direction to speed up in
         */
        void speed_up(Direction direction);
        /**
         * @brief Slow down the entity in given direction
         * @param direction Direction to slow down in
         */
        void slow_down(Direction direction);
        /**
         * @brief Progresses entity one tick further
         * @param entity_list List of all entities in the model, used when entities create other entities e.g. bullets
         */
        virtual void tick(std::list<std::shared_ptr<Model::Entity>>& entity_list);
        /**
         * @brief Checks if the entity is in or out of view
         * @return true if entity is visible, false otherwise
         */
        bool in_fov() const;
        /**
         * @brief Notifies the entity's observers
         */
        void notify_observers() override;
        /**
         * @brief Marks the entity for destruction
         */
        void destroy();
        /**
         * @brief Checks whether or not the entity should be destroyed
         * @return true if entity should be destroyed, false otherwise
         */
        virtual bool is_destroyed() const;
        /**
         * @brief Indicates the type of the entity
         * @return EntityType enum value indicating the type of the entity
         */
        virtual EntityType get_type() const = 0;
        /**
         * @brief Indicates whether or not collision detection should be performed for this entity
         * @return true if collision detection should be performed, false otherwise
         */
        bool is_collidable() const;
        /**
         * @brief Indicates whether or not the entity is friendly or an enemy
         * @return true if entity is friendly, false otherwise
         */
        bool is_friendly() const;

    protected:
        /**
         * @brief Default constructor
         */
        Entity();
        /**
         * @brief Construct the entity at given location, with given contact radius
         * @param init_x initial position on x-axis
         * @param init_y initial position on y-axis
         * @param radius contact radius/hitbox size for entity
         */
        Entity(float init_x, float init_y, int z, float radius);
        /**
         * @brief Pure virtual destructor, prevents direct instantiation of Entity
         */
        ~Entity() override = 0;
        /**
         * @brief Copy constructor
         * @details Copy constructor, protected to prevent unauthorised copies
         */
        Entity(Entity const& entity) = default;
        /**
         * @brief Assignment operator
         * @details Assignment operator, protected to prevent unauthorised assignment
         */
        Entity& operator=(Entity const& entity) = default;
        /**
         * @brief Notifies observers of a location update
         */
        void notify_location_update() const;

        float x;
        float y;
        int z = 0;
        float size;
        float x_speed = 0;
        float y_speed = 0;
        bool collidable = true;
        bool friendly = true;
        bool deleted = false;

    };
}

#endif //GRADIUS_ENTITY_HPP
