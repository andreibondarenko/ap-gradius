#ifndef GRADIUS_HEALTHPOINT_HPP
#define GRADIUS_HEALTHPOINT_HPP

#include "Entity.hpp"

namespace Model
{
    class HealthPoint : public Entity
    {
    public:
        /**
         * @brief Default constructor
         */
        HealthPoint();
        /**
         * @brief Constructs health point at given location
         * @param init_x initial position on x-axis
         * @param init_y initial position on y-axis
         */
        HealthPoint(float init_x, float init_y);
        /**
         * @brief Destructor
         */
        ~HealthPoint() override = default;
        /**
         * @brief Copy constructor
         */
        HealthPoint(HealthPoint const& hp) = default;
        /**
         * @brief Assignment operator
         */
        HealthPoint& operator=(HealthPoint const& hp) = default;
        /**
         * @brief Progresses entity one tick further
         * @param entity_list List of all entities in the model, used when entities create other entities e.g. bullets
         */
        void tick(std::list<std::shared_ptr<Model::Entity>> &entity_list) override;
        /**
         * @brief Indicates the type of the entity
         * @return EntityType enum value indicating instance is a health point entity
         */
        EntityType get_type() const override;
    };
}

#endif //GRADIUS_HEALTHPOINT_HPP
