#include "Bullet.hpp"
#include "../Settings.hpp"

Model::Bullet::Bullet() : Entity(0,
                                 0,
                                 Settings::get_instance().GAME_Z,
                                 Settings::get_instance().SIZE_BULLETS)
{
    enemy_or_player = EntityType::player_bullet_entity;
}

Model::Bullet::Bullet(float init_x, float init_y, bool enemy) : Entity(init_x,
                                                                       init_y,
                                                                       Settings::get_instance().GAME_Z,
                                                                       Settings::get_instance().SIZE_BULLETS)
{
    if (enemy)
    {
        enemy_or_player = EntityType::enemy_bullet_entity;
        friendly = false;
        x_speed = -Settings::get_instance().MOVEMENT_SPEED_BULLETS;
    }
    else
    {
        enemy_or_player = EntityType::player_bullet_entity;
        x_speed = Settings::get_instance().MOVEMENT_SPEED_BULLETS;
    }
}

Model::EntityType Model::Bullet::get_type() const
{
    return enemy_or_player;
}

void Model::Bullet::tick(std::list<std::shared_ptr<Model::Entity>> &entity_list)
{
    Entity::tick(entity_list);
    if (x > Settings::get_instance().MODEL_WIDTH/2.0f || x < -Settings::get_instance().MODEL_WIDTH/2.0f)
        deleted = true;
}
