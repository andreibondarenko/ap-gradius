#ifndef GRADIUS_WORLD_HPP
#define GRADIUS_WORLD_HPP

#include <memory>
#include <list>
#include <string>
#include "Player.hpp"

class Controller;

namespace View
{
    class Window;
}

namespace Model
{
    /// Enum of user commands supported by Model
    enum class Command
    {
        start_moving_up,    /// Start moving the player up
        start_moving_down,  /// Start moving the player down
        start_moving_left,  /// Start moving the player left
        start_moving_right, /// Start moving the player right
        stop_moving_up,     /// Stop moving the player up
        stop_moving_down,   /// Stop moving the player down
        stop_moving_left,   /// Stop moving the player left
        stop_moving_right,  /// Stop moving the player right
        player_shoot        /// Make the player shoot a projectile
    };


    /// Class used for running the model of the game
    class Game
    {
    public:
        /**
         * @brief Default constructor
         */
        Game();
        /**
         * @brief Initializes model with level described in file
         * @param lvl_file Level file to load from
         */
        bool load_from_file(std::string const& lvl_file);
        /**
         * @brief Executes the given command
         * @param command Command to be performed
         */
        void execute_command(Command command);
        /**
         * @brief Runs the model
         */
        bool run();
        /**
         * @brief Stops the model if it's running
         */
        void stop();
        /**
         * @brief Links the controller which processes the input events
         * @param c Controller to be linked
         */
        void link_controller(Controller& c);
        /**
         * @brief Links the window which represents the model
         * @param w View to be linked
         */
        void link_view(View::Window &w);

    private:
        std::shared_ptr<Player> player;
        std::list<std::shared_ptr<Model::Entity>> entities;
        /**
         * Reason for using raw pointer over shared pointer:
         * Since controller is instantiated separately, destruction will be taken care of
         * by the controller itself when exiting main.
         * If we create a shared pointer to an already instantiated object, we will
         * get Malloc errors when the destructor of the shared pointer is invoked
         */
        Controller* control = nullptr;
        View::Window* window = nullptr;
        bool running = false;
        float end_marker = 0;

        /**
         * @brief Detects collisions between any of the entities
         */
        void detect_collisions();
        /**
         * @brief Handles a collision between two given entities
         * @param ent1 First entity involved in collision
         * @param ent2 Second entity involved in collision
         */
        void handle_collision(std::shared_ptr<Model::Entity> ent1, std::shared_ptr<Model::Entity> ent2);
    };
}

#endif //GRADIUS_WORLD_HPP
