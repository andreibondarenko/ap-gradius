#include "Background.hpp"
#include "../Settings.hpp"

Model::Background::Background() : Entity(0,
                                         0,
                                         Settings::get_instance().BACKGROUND_Z,
                                         Settings::get_instance().MODEL_HEIGHT/2.0f)
{
    collidable = false;
    x_speed = -Settings::get_instance().SCROLL_SPEED;
}

Model::Background::Background(float init_x, float init_y) : Entity(init_x,
                                                                   init_y,
                                                                   Settings::get_instance().BACKGROUND_Z,
                                                                   Settings::get_instance().MODEL_HEIGHT/2.0f)
{
    collidable = false;
    x_speed = -Settings::get_instance().SCROLL_SPEED;
}

void Model::Background::tick(std::list<std::shared_ptr<Model::Entity>> &entity_list)
{
    Entity::tick(entity_list);
    if (x <= -Settings::get_instance().MODEL_WIDTH/2.0f - Settings::get_instance().MODEL_HEIGHT/2.0f)
        x = - Settings::get_instance().MODEL_WIDTH/2.0f
            + 2*Settings::get_instance().MODEL_HEIGHT
            + Settings::get_instance().MODEL_HEIGHT/2.0f;
}

Model::EntityType Model::Background::get_type() const
{
    return EntityType::background_entity;
}
