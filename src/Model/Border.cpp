#include "Border.hpp"
#include "../Settings.hpp"

Model::Border::Border() : Entity(0,
                                 0,
                                 Settings::get_instance().GAME_Z,
                                 Settings::get_instance().SIZE_BORDER)
{
    friendly = false;
    x_speed = -Settings::get_instance().SCROLL_SPEED;
}

Model::Border::Border(float init_x, float init_y) : Entity(init_x,
                                                           init_y,
                                                           Settings::get_instance().GAME_Z,
                                                           Settings::get_instance().SIZE_BORDER)
{
    friendly = false;
    x_speed = -Settings::get_instance().SCROLL_SPEED;
}

void Model::Border::tick(std::list<std::shared_ptr<Model::Entity>> &entity_list)
{
    Entity::tick(entity_list);
    if (x <= -Settings::get_instance().MODEL_WIDTH/2.0f - Settings::get_instance().SIZE_BORDER/2.0f)
        x = Settings::get_instance().MODEL_WIDTH/2.0f + Settings::get_instance().SIZE_BORDER/2.0f;
}

Model::EntityType Model::Border::get_type() const
{
    if (y > 0)
        return EntityType::top_border_entity;
    else
        return EntityType::bottom_border_entity;
}
