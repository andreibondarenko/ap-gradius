#include "Player.hpp"
#include "Bullet.hpp"
#include "../Settings.hpp"

Model::Player::Player() : Entity(0,
                                 0,
                                 Settings::get_instance().GAME_Z,
                                 Settings::get_instance().SIZE_SHIPS)
{}

Model::Player::Player(float init_x, float init_y) : Entity(init_x,
                                                           init_y,
                                                           Settings::get_instance().GAME_Z,
                                                           Settings::get_instance().SIZE_SHIPS)
{}

std::shared_ptr<Model::Entity> Model::Player::shoot() const
{
    return std::shared_ptr<Model::Bullet>(new Bullet(this->x + this->size, this->y));
}

Model::EntityType Model::Player::get_type() const
{
    return EntityType::player_entity;
}

void Model::Player::tick(std::list<std::shared_ptr<Model::Entity>> &entity_list)
{
    if ((x + size < Settings::get_instance().MODEL_WIDTH/2.0f && x_speed > 0)
        ||
        (x - size > -Settings::get_instance().MODEL_WIDTH/2.0f && x_speed < 0))
    {
        x += x_speed;
    }
    y += y_speed;
    if (x < -Settings::get_instance().MODEL_WIDTH/2.0f)
        while (!health_points.empty()) health_points.pop();
}

std::shared_ptr<Model::Entity> Model::Player::add_health_point()
{
    std::shared_ptr<Model::HealthPoint> point(new HealthPoint(Settings::get_instance().HP_X_POS[health_points.size()],
                                                              Settings::get_instance().HP_Y_POS));
    if (health_points.size() <= Settings::get_instance().MAX_HP)
        health_points.push(point);
    return point;
}

void Model::Player::remove_health_point(int points)
{
    if (points > health_points.size())
    {
        while (!health_points.empty())
        {
            health_points.top()->destroy();
            health_points.pop();
        }
    }
    else
    {
        for (int i = 0; i < points; ++i)
        {
            health_points.top()->destroy();
            health_points.pop();
        }
    }
}

void Model::Player::rebound(Direction rebound_direction)
{
    switch (rebound_direction)
    {
        case Direction::up:
            y += 0.2f;
            break;
        case Direction::down:
            y -= 0.2f;
            break;
        case Direction::left:
            x -= 1.0f;
            break;
        case Direction::right:
            x += 0.1f;
            break;
    }
}

bool Model::Player::is_alive()
{
    return !health_points.empty();
}

