#ifndef GRADIUS_BACKGROUND_HPP
#define GRADIUS_BACKGROUND_HPP

#include "Entity.hpp"

namespace Model
{
    class Background : public Entity
    {
    public:
        /**
         * @brief Default constructor
         */
        Background();
        /**
         * @brief Construct background at given location
         * @param init_x initial position on x-axis
         * @param init_y initial position on y-axis
         */
        Background(float init_x, float init_y);
        /**
         * @brief Destructor
         */
        ~Background() override = default;
        /**
         * @brief Copy constructor
         */
        Background(Background const& background) = default;
        /**
         * @brief Assignment operator
         */
        Background& operator=(Background const& background) = default;
        /**
         * @brief Progresses entity one tick further
         * @param entity_list List of all entities in the model, used when entities create other entities e.g. bullets
         */
        void tick(std::list<std::shared_ptr<Entity>> &entity_list) override;
        /**
         * @brief Indicates the type of the entity
         * @return EntityType enum value indicating instance is a background entity
         */
        EntityType get_type() const override;
    };
}

#endif //GRADIUS_BACKGROUND_HPP
