#include "HealthPoint.hpp"
#include "../Settings.hpp"

Model::HealthPoint::HealthPoint() : Entity(0,
                                           0,
                                           Settings::get_instance().UI_Z,
                                           Settings::get_instance().SIZE_HP)
{
    collidable = false;
}

Model::HealthPoint::HealthPoint(float init_x, float init_y) : Entity(init_x,
                                                                     init_y,
                                                                     Settings::get_instance().UI_Z,
                                                                     Settings::get_instance().SIZE_HP)
{
    collidable = false;
}

void Model::HealthPoint::tick(std::list<std::shared_ptr<Model::Entity>> &entity_list)
{}

Model::EntityType Model::HealthPoint::get_type() const
{
    return EntityType::hp_entity;
}
