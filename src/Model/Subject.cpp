#include "Subject.hpp"

void Abstract::Subject::attach_observer(std::shared_ptr<Abstract::Observer> new_observer)
{
    observer_collection.push_back(new_observer);
}

bool Abstract::Subject::has_observers() const
{
    return !observer_collection.empty();
}

void Abstract::Subject::clear_observers()
{
    observer_collection.clear();
}

Abstract::Subject::~Subject() = default;
