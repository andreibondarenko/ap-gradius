#ifndef GRADIUS_BORDER_HPP
#define GRADIUS_BORDER_HPP

#include "Entity.hpp"

namespace Model
{
    class Border : public Entity
    {
    public:
        /**
         * @brief Default constructor
         */
        Border();
        /**
         * @brief Construct background at given location
         * @param init_x initial position on x-axis
         * @param init_y initial position on y-axis
         */
        Border(float init_x, float init_y);
        /**
         * @brief Destructor
         */
        ~Border() override = default;
        /**
         * @brief Copy constructor
         */
        Border(Border const& b) = default;
        /**
         * @brief Assignment operator
         */
        Border& operator=(Border const& b) = default;
        /**
         * @brief Progresses entity one tick further
         * @param entity_list List of all entities in the model, used when entities create other entities e.g. bullets
         */
        void tick(std::list<std::shared_ptr<Entity>> &entity_list) override;
        /**
         * @brief Indicates the type of the entity
         * @return EntityType enum value indicating instance is a border entity
         */
        EntityType get_type() const override;
    };
}

#endif //GRADIUS_BORDER_HPP
