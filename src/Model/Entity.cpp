#include "Entity.hpp"
#include "../Settings.hpp"

float Model::Entity::get_x() const
{
    return x;
}

float Model::Entity::get_y() const
{
    return y;
}

int Model::Entity::get_z() const
{
    return z;
}

float Model::Entity::get_size() const
{
    return size;
}

float Model::Entity::get_x_speed() const
{
    return x_speed;
}

float Model::Entity::get_y_speed() const
{
    return y_speed;
}

void Model::Entity::speed_up(Model::Direction direction)
{
    switch (direction)
    {
        case Direction::up:
            y_speed += Settings::get_instance().MOVEMENT_SPEED_SHIPS;
            break;
        case Direction::down:
            y_speed -= Settings::get_instance().MOVEMENT_SPEED_SHIPS;
            break;
        case Direction::left:
            x_speed -= Settings::get_instance().MOVEMENT_SPEED_SHIPS;
            break;
        case Direction::right:
            x_speed += Settings::get_instance().MOVEMENT_SPEED_SHIPS;
            break;
    }
}

void Model::Entity::slow_down(Model::Direction direction)
{
    switch (direction)
    {
        case Direction::up:
            y_speed -= Settings::get_instance().MOVEMENT_SPEED_SHIPS;
            break;
        case Direction::down:
            y_speed += Settings::get_instance().MOVEMENT_SPEED_SHIPS;
            break;
        case Direction::left:
            x_speed += Settings::get_instance().MOVEMENT_SPEED_SHIPS;
            break;
        case Direction::right:
            x_speed -= Settings::get_instance().MOVEMENT_SPEED_SHIPS;
            break;
    }
}

void Model::Entity::tick(std::list<std::shared_ptr<Model::Entity>> &entity_list)
{
    x += x_speed;
    y += y_speed;
}

bool Model::Entity::in_fov() const
{
    /**
     * Model is a fixed [-4,4]x[-3,3] field
     * This is the reason for the use of float literals in calculations
     */
    return  (x - size <= Settings::get_instance().MODEL_WIDTH/2.0f) &&
            (x + size >= -Settings::get_instance().MODEL_WIDTH/2.0f) &&
            (y - size <= Settings::get_instance().MODEL_HEIGHT/2.0f) &&
            (y + size >= -Settings::get_instance().MODEL_HEIGHT/2.0f);
}

void Model::Entity::notify_observers()
{
    notify_location_update();
}

void Model::Entity::destroy()
{
    deleted = true;
}

bool Model::Entity::is_destroyed() const
{
    return deleted;
}

bool Model::Entity::is_collidable() const
{
    return collidable;
}

bool Model::Entity::is_friendly() const
{
    return friendly;
}

Model::Entity::Entity() : x(0), y(0), size(0)
{}

Model::Entity::Entity(float init_x, float init_y, int z, float radius) : x(init_x), y(init_y), z(z), size(radius)
{}

Model::Entity::~Entity() = default;

void Model::Entity::notify_location_update() const
{
    for (const auto &observer: observer_collection) observer->update_location(x, y);
}
