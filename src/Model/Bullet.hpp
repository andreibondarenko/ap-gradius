#ifndef GRADIUS_BULLET_HPP
#define GRADIUS_BULLET_HPP

#include "Entity.hpp"

namespace Model
{
    class Bullet : public Entity
    {
    public:
        /**
         * @brief Default constructor
         */
        Bullet();
        /**
         * @brief Construct background at given location
         * @param init_x Initial position on x-axis
         * @param init_y Initial position on y-axis
         * @param enemy Bool indicating whether or not the bullet was created by an enemy
         */
        Bullet(float init_x, float init_y, bool enemy=false);
        /**
         * @brief Destructor
         */
        ~Bullet() override = default;
        /**
         * @brief Copy constructor
         */
        Bullet(Bullet const& pb) = default;
        /**
         * @brief Assignment operator
         */
        Bullet& operator=(Bullet const& pb) = default;
        /**
         * @brief Progresses entity one tick further
         * @param entity_list List of all entities in the model, used when entities create other entities e.g. bullets
         */
        void tick(std::list<std::shared_ptr<Entity>> &entity_list) override;
        /**
         * @brief Indicates the type of the entity
         * @return EntityType enum value indicating instance is a bullet entity
         */
        EntityType get_type() const override;

    private:
        EntityType enemy_or_player;
    };
}

#endif //GRADIUS_BULLET_HPP
