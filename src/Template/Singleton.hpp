#ifndef GRADIUS_SINGLETON_HPP
#define GRADIUS_SINGLETON_HPP

template<class T>
class Singleton
{
public:
    /**
     * @brief Returns reference to singleton
     * @return reference to singleton class instance
     */
    static T& get_instance()
    {
        static T instance;
        return instance;
    }
    /**
     * @brief Destructor
     */
    ~Singleton() = default;
    /**
     * @brief Copy constructor
     * @details Copy constructor, deleted to satisfy singleton pattern
     */
    Singleton(Singleton const& s) = delete;
    /**
     * @brief Assignment operator
     * @details Assignment operator, deleted to satisfy singleton pattern
     */
    Singleton& operator=(Singleton const& s) = delete;

protected:
    /**
     * @brief Default constructor
     * @details Default constructor, protected to satisfy singleton pattern and enable inheritance
     */
    Singleton() = default;

};

#endif //GRADIUS_SINGLETON_HPP
