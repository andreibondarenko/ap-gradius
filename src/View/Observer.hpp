#ifndef GRADIUS_OBSERVER_HPP
#define GRADIUS_OBSERVER_HPP

#include <memory>
#include <utility>

namespace Abstract
{

    class Observer
    {
    public:
        /**
         * @brief Notifies observer of a location update
         */
        virtual void update_location(float x, float y) = 0;

    protected:
        /**
         * @brief Default constructor
         */
        Observer() = default;
        /**
         * @brief Pure virtual destructor, prevents direct instantiation of Observer
         */
        virtual ~Observer() = 0;
        /**
         * @brief Copy constructor
         */
        Observer(Observer const& observer) = default;
        /**
         * @brief Assignment operator
         */
        Observer& operator=(Observer const& observer) = default;
    };
}

#endif //GRADIUS_OBSERVER_HPP
