#ifndef GRADIUS_WINDOW_HPP
#define GRADIUS_WINDOW_HPP

#include <memory>
#include <SFML/Graphics.hpp>
#include "Sprite.hpp"
#include <list>

class Controller;

namespace Model
{
    class Entity;
}

namespace View
{

    class Window
    {
    public:
        /**
         * @brief Default constructor
         */
        Window() = default;
        /**
         * @brief Deleted copy constructor to prevent copies
         */
        Window(Window const& window) = delete;
        /**
         * @brief Deleted assignment operator to prevent assignment
         */
        Window& operator=(Window const& window) = delete;
        /**
         * @brief Opens the window
         * @param ctrl controller to take care of user input
         */
        void run();
        /**
         * @brief Links controller to feed input events to
         * @param ctrl controllet to link
         */
        void link_controller(Controller& ctrl);
        /**
         * @brief Creates visual representation for a model entity
         * @param subject model entity to create representation for
         */
        void create_representation(std::shared_ptr<Model::Entity> const& subject);
        /**
         * @brief Closes the window
         */
        void terminate();

    private:
        std::unique_ptr<sf::RenderWindow> sf_window = nullptr;
        bool running = false;
        /**
         * Reason for using raw pointer over shared pointer:
         * Since controller is instantiated separately, destruction will be taken care of
         * by the controller itself when exiting main.
         * If we create a shared pointer to an already instantiated object, we will
         * get Malloc errors when the destructor of the shared pointer is invoked
         */
        Controller* control = nullptr;
        std::list<std::shared_ptr<View::Sprite>> ui_elements;
        std::list<std::shared_ptr<View::Sprite>> game_elements;
        std::list<std::shared_ptr<View::Sprite>> background_elements;

        /**
         * @brief Draws all sprites in the window
         */
        void draw_sprites();

    };
}

#endif //GRADIUS_WINDOW_HPP
