#include "Window.hpp"
#include "../Controller.hpp"
#include "../Model/Entity.hpp"
#include "../Settings.hpp"

void View::Window::run()
{
    unsigned int horizontal_resolution = Settings::get_instance().resolution_width;
    unsigned int vertical_resolution = Settings::get_instance().resolution_height;
    bool full_screen_enabled = Settings::get_instance().full_screen_enabled;
    if (!running && sf_window == nullptr)
    {
        running = true;
        sf_window = std::unique_ptr<sf::RenderWindow>(
                new sf::RenderWindow(sf::VideoMode(horizontal_resolution, vertical_resolution), "Gradius", 1 << (2 + full_screen_enabled)));
        sf_window->setKeyRepeatEnabled(false);
        while (sf_window->isOpen() && running)
        {
            sf::Event event;
            while (sf_window->pollEvent(event))
            {
                if (event.type == sf::Event::Closed)
                {
                    sf_window->close();

                    control->terminate_model();
                }

                if (event.type == sf::Event::KeyPressed || event.type == sf::Event::KeyReleased)
                    control->register_event(event);
            }
            sf_window->clear();
            draw_sprites();
            sf_window->display();
        }
    }
}

void View::Window::link_controller(Controller &ctrl)
{
    control = &ctrl;
}

void View::Window::create_representation(std::shared_ptr<Model::Entity> const& subject)
{
    std::shared_ptr<View::Sprite> sprite_ptr;
    float x = subject->get_x();
    float y = subject->get_y();
    int z = subject->get_z();
    float cr = subject->get_size();
    Model::EntityType type = subject->get_type();
    switch (type)
    {
        case Model::EntityType::player_entity :
            sprite_ptr = std::shared_ptr<View::Sprite>(new View::Sprite(x, y, 2.0f*cr, TextureType::player_ship));
            break;
        case Model::EntityType::player_bullet_entity :
            sprite_ptr = std::shared_ptr<View::Sprite>(new View::Sprite(x, y, 2.0f*cr, TextureType::player_bullet));
            break;
        case Model::EntityType::background_entity :
            sprite_ptr = std::shared_ptr<View::Sprite>(new View::Sprite(x, y, 2.0f*cr, TextureType::background));
            break;
        case Model::EntityType::top_border_entity :
            sprite_ptr = std::shared_ptr<View::Sprite>(new View::Sprite(x, y, 2.0f*cr, TextureType::top_border));
            break;
        case Model::EntityType::bottom_border_entity :
            sprite_ptr = std::shared_ptr<View::Sprite>(new View::Sprite(x, y, 2.0f*cr, TextureType::bottom_border));
            break;
        case Model::EntityType::hp_entity :
            sprite_ptr = std::shared_ptr<View::Sprite>(new View::Sprite(x, y, 2.0f*cr, TextureType::health_point));
            break;
        case Model::EntityType::obstacle_entity :
            sprite_ptr = std::shared_ptr<View::Sprite>(new View::Sprite(x, y, 2.0f*cr, TextureType::obstacle));
            break;
        case Model::EntityType::enemy_fighter_entity :
            sprite_ptr = std::shared_ptr<View::Sprite>(new View::Sprite(x, y, 2.0f*cr, TextureType::enemy_fighter));
            break;
        case Model::EntityType::enemy_ufo_entity :
            sprite_ptr = std::shared_ptr<View::Sprite>(new View::Sprite(x, y, 2.0f*cr, TextureType::enemy_ufo));
            break;
        case Model::EntityType::enemy_bullet_entity :
            sprite_ptr = std::shared_ptr<View::Sprite>(new View::Sprite(x, y, 2.0f*cr, TextureType::enemy_bullet));
            break;
        default:
            break;
    }
    subject->attach_observer(sprite_ptr);
    switch (z)
    {
        case -1:
            background_elements.push_back(sprite_ptr);
            break;
        case 0:
            game_elements.push_back(sprite_ptr);
            break;
        case 1:
            ui_elements.push_back(sprite_ptr);
            break;
        default:
            break;
    }
}

void View::Window::draw_sprites()
{
    for (auto spriteIt = background_elements.begin();  spriteIt != background_elements.end();)
    {
        if (spriteIt->unique())
        {
            spriteIt = background_elements.erase(spriteIt);
        }
        else
        {
            (*spriteIt)->draw(*sf_window);
            ++spriteIt;
        }
    }
    for (auto spriteIt = game_elements.begin();  spriteIt != game_elements.end();)
    {
        if (spriteIt->unique())
        {
            spriteIt = game_elements.erase(spriteIt);
        }
        else
        {
            (*spriteIt)->draw(*sf_window);
            ++spriteIt;
        }
    }
    for (auto spriteIt = ui_elements.begin();  spriteIt != ui_elements.end();)
    {
        if (spriteIt->unique())
        {
            spriteIt = ui_elements.erase(spriteIt);
        }
        else
        {
            (*spriteIt)->draw(*sf_window);
            ++spriteIt;
        }
    }
}

void View::Window::terminate()
{
    running = false;
}


