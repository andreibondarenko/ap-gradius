#ifndef GRADIUS_REPRESENTATION_HPP
#define GRADIUS_REPRESENTATION_HPP

#include <SFML/Graphics.hpp>
#include "Observer.hpp"

namespace View
{

    enum class TextureType
    {
        background,
        player_ship,
        player_bullet,
        top_border,
        bottom_border,
        health_point,
        obstacle,
        enemy_fighter,
        enemy_ufo,
        enemy_bullet
    };
    
    class Sprite : public Abstract::Observer
    {
    public:
        /**
         * @brief Default constructor
         */
        Sprite() = default;
        /**
         * @brief Constructs a sprite at given position with given size and texture
         * @param model_x initial position on x-axis
         * @param model_y initial position on y-axis
         * @param model_size size of the entity in the model
         * @param texture TextureType to use for the sprite
         */
        Sprite(float model_x, float model_y, float model_size, TextureType texture);
        /**
         * @brief Copy constructor
         */
        Sprite(Sprite const& s) = default;
        /**
         * @brief Assignment operator
         */
        Sprite& operator=(Sprite const& s) = default;
        /**
         * @brief Pure virtual destructor, prevents direct instantiation of Sprite
         */
        ~Sprite() override = default;
        /**
         * @brief Draws the sprite in given render window
         * @param window window to draw in
         */
        void draw(sf::RenderWindow& window) const;
        /**
         * @brief Updates the location of the observer
         * @param x new position on x-axis
         * @param y new position on y-axis
         */
        void update_location(float x, float y) override;
    private:
        sf::Texture sf_texture;
        sf::Sprite sf_sprite;
    };
}


#endif //GRADIUS_REPRESENTATION_HPP
