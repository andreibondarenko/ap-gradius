#include "../Utility/Transform.hpp"
#include "Sprite.hpp"
#include <iostream>

View::Sprite::Sprite(float model_x, float model_y, float model_size, TextureType texture)
{
    // Select correct texture resource
    std::string texture_file;
    switch (texture)
    {
        case TextureType::background :
            texture_file = "res/textures/background.png";
            break;
        case TextureType::player_ship :
            texture_file = "res/textures/player.png";
            break;
        case TextureType::top_border :
            texture_file = "res/textures/border_top.png";
            break;
        case TextureType::bottom_border :
            texture_file = "res/textures/border_bottom.png";
            break;
        case TextureType::player_bullet:
            texture_file = "res/textures/player_bullet.png";
            break;
        case TextureType::health_point:
            texture_file = "res/textures/health_point.png";
            break;
        case TextureType::obstacle:
            texture_file = "res/textures/obstacle.png";
            break;
        case TextureType::enemy_fighter:
            texture_file = "res/textures/enemy_fighter.png";
            break;
        case TextureType::enemy_ufo:
            texture_file = "res/textures/enemy_ufo.png";
            break;
        case TextureType::enemy_bullet:
            texture_file = "res/textures/enemy_bullet.png";
            break;
    }
    // Load texture from file
    if (!sf_texture.loadFromFile(texture_file))
    {
        std::cout << "Resource missing!\n--> Could not find: "
                  << texture_file
                  << std::endl;
    }
    sf_texture.setSmooth(true);
    sf_sprite.setTexture(sf_texture);
    // Scale sprite:
    int texture_size = sf_texture.getSize().x;
    float x_scale_factor = Utility::Transform::get_instance().get_x_scale_factor(model_size, texture_size);
    float y_scale_factor = Utility::Transform::get_instance().get_y_scale_factor(model_size, texture_size);
    sf_sprite.scale(x_scale_factor, y_scale_factor);
    // Center origin of the sprite
    sf::FloatRect bound_rect = sf_sprite.getLocalBounds();
    sf_sprite.setOrigin(bound_rect.left + bound_rect.width/2.0f,
                        bound_rect.top  + bound_rect.height/2.0f);
    // Convert model coordinates to view coordinates
    float view_x = Utility::Transform::get_instance().x_model_to_view(model_x);
    float view_y = Utility::Transform::get_instance().y_model_to_view(model_y);
    // Set position of the sprite
    sf_sprite.setPosition(view_x, view_y);
}

void View::Sprite::draw(sf::RenderWindow &window) const
{
    window.draw(sf_sprite);
}

void View::Sprite::update_location(float x, float y) {
    float new_x = Utility::Transform::get_instance().x_model_to_view(x);
    float new_y = Utility::Transform::get_instance().y_model_to_view(y);
    sf_sprite.setPosition(new_x, new_y);
}

