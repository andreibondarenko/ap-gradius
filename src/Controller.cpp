#include "Controller.hpp"
#include "Model/Game.hpp"

void Controller::register_event(sf::Event new_event)
{
    this->event_queue.push(new_event);
}

void Controller::link_model(Model::Game& new_model)
{
    // Reason for using raw pointer --> see header
    model = &new_model;
}


void Controller::process_next_event()
{
    if (!event_queue.empty())
    {
        sf::Event next_event = event_queue.front();
        if (next_event.type == sf::Event::KeyPressed)
        {
            switch (next_event.key.code)
            {
                case sf::Keyboard::Up:
                    model->execute_command(Model::Command::start_moving_up);
                    break;
                case sf::Keyboard::Down:
                    model->execute_command(Model::Command::start_moving_down);
                    break;
                case sf::Keyboard::Left:
                    model->execute_command(Model::Command::start_moving_left);
                    break;
                case sf::Keyboard::Right:
                    model->execute_command(Model::Command::start_moving_right);
                    break;
                case sf::Keyboard::Space:
                    model->execute_command(Model::Command::player_shoot);
                    break;
                default:
                    break;
            }
        }
        else if (next_event.type == sf::Event::KeyReleased)
        {
            switch (next_event.key.code)
            {
                case sf::Keyboard::Up:
                    model->execute_command(Model::Command::stop_moving_up);
                    break;
                case sf::Keyboard::Down:
                    model->execute_command(Model::Command::stop_moving_down);
                    break;
                case sf::Keyboard::Left:
                    model->execute_command(Model::Command::stop_moving_left);
                    break;
                case sf::Keyboard::Right:
                    model->execute_command(Model::Command::stop_moving_right);
                    break;
                default:
                    break;
            }
        }
        event_queue.pop();
    }
}

void Controller::terminate_model()
{
    model->stop();
}
