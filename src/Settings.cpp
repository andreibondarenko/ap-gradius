#include "Settings.hpp"
#include "Lib/json.hpp"
#include <fstream>

using json = nlohmann::json;

bool Settings::load_from_file(std::string const &filename)
{
    // START Parse .json input file
    json data;
    std::ifstream JSON(filename);
    if (JSON.is_open())
    {
        JSON >> data;
        JSON.close();
    }
    else
    {
        std::cout << "Unable to open file: "<< filename << std::endl;
        return false;
    }
    // END Parse .json input file

    try
    {
        this->resolution_width = data.at("resolution").at("horizontal");
        this->resolution_height = data.at("resolution").at("vertical");
        this->full_screen_enabled = data.at("full_screen_enabled");
    }
    catch (std::out_of_range const& oor)
    {
        this->resolution_width = 800;
        this->resolution_height = 600;
        this->full_screen_enabled = false;
        std::cout << "Settings in \"" << filename << "\" invalid, reverted to default settings." << std::endl;
    }
    return true;
}