# Project: Gradius

> Project: Gradius  
> Course: Advanced Programming  
> University: University of Antwerp  
> Professor: Jan Broeckhove  
> Assistant: Glenn Daneels  
> Student: Andrei Bondarenko  
> Student ID: 20151524  

## 0. Settings
Game resolution and full screen can be set in ./res/settings.json

## 1. Building and running
Just make the run.sh script executable and run, it will take care of everything. Once the build is done just follow the instructions on screen.

## 2. Controls
* arrow keys: navigate the player's ship around the screen
* spacebar: shoot a bullet

## 3. Game Rules
**Goal:** Avoid/destroy enemy ships, bullets and obstacles and reach the end of the level by navigating your ship around the screen and shooting at oncoming enemies.</br>
Losing all of your HP beforing finishing the level results in a loss.

You will face following entities:

* Border:
	* Located at the top and bottom of the screen
	* Cannot be destroyed
	* If hit, player loses 2 HP  
	![](res/textures/border_top.png)  
	![](res/textures/border_bottom.png)
* Obstacle:
	* Cannot be destroyed
	* If hit, player loses 1 HP  
	![](res/textures/obstacle.png)
* UFO: 
	* Floats by (usually in groups)
	* Requires 2 bullets to destroy
	* If hit, gets destroyed and player loses 1 HP  
	![](res/textures/enemy_ufo.png)
* Fighter: 
	* Bounces up and down on the screen
	* Sporadically shoots bullets
	* Requires 1 bullet to destroy
	* If hit, gets destroyed and player loses 1 HP  
	![](res/textures/enemy_fighter.png)
* Bullet: 
	* Shot by fighter
	* Cannot be destroyed with bullets
	* If hit, gets destroyed and player loses 1 HP  
	![](res/textures/enemy_bullet.png)

